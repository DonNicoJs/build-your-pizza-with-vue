export const ingredients = [
  {id: 1, name: 'Ciabatte bucate', allergens: [1, 2, 3], icon: 'banana.png'},
  {id: 2, name: 'Calzini usati',  allergens: [3, 4, 5], icon: 'chicken.png'},
  {id: 3, name: 'Tovagliolo strappato',  allergens: [1, 2, 3], icon: 'pineapple.png'},
  {id: 4, name: 'Nutella',  allergens: [1, 5, 2], icon: 'pizza-boxes.png'},
  {id: 5, name: 'Cacio cavallo ammuffito',  allergens: [3, 1, 4], icon: 'cherry.png'},
  {id: 6, name: 'Parmigiano leggiano (contraffatto)',  allergens: [1, 2, 3], icon: 'cook-book.png'}
]

export const allergens =  [
  {id: 1, name: 'Andare all lavoro', icon: 'cook-book.png'},
  {id: 2, name: 'I compiti a casa', icon: 'chicken.png' },
  {id: 3, name: 'Le riunioni il Venerdi sera', icon: 'pizza-boxes.png'},
  {id: 4, name: 'La pasta scotta', icon: 'cherry.png'},
  {id: 5, name: 'Gli spaghetti spezzati', icon: 'pineapple.png'}
]