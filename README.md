# Vue Apropos Workshop

> Teoria e pratica sul construire interfacce con Vue

https://slides.com/donnicojs/apropos-vue-js-basic-workshop

## Requisiti

- Un computer
- Una connessione at internet
- Conoscenza basilare di Javascript, HTML e CSS
- Curiosità e voglia di imparare

## Disclaimer

Vi prego di interrompermi in ogni momento, qualsiasi domanda è lecita

## Tabella dei contenuti

- Anatomia del componente Vue
- Reattività: concetti base e dettagli importanti
- Comunicazione fra componenti e come "componentizzare" efficientemente
- Pattern base di architettura: da semplici app a complesse SPA
- Vue router e Vuex: introduzione
- Unit test: concetti base ed esempi

## Gli esercizi

Vista la natura online di questo workshop e il numero cospicuo di attendenti utilizzeremo la tecnica del mob programming, il docente sarà sempre alla tastiera e gli alievi faranno a turni per dettare/discutere il codice.
