# Unit test: concetti base ed esempi

Nessun codice può dirsi finito e consegnabile se non è corredato da `unit test`. 

Vediamo ora alcuni concetti chiave e qualche esempio.

## Jest

Ci sono varie soluzioni per avere degli `unit test` compatibili con VueJs, ma l'accoppiata raccomandata è `jest` e `vue-test-utils`.

Vediamo brevemente da quali elementi è composto un test scritto in `jest`:

```javascript

describe('il componente sotto test', () => {
  beforeEach(() => {
    mountComponent();
  });

  it('renders an icon', () => {
    expect(something).toBe('whatItShouldBe')
  })
})
```

Analizziamo i componenti di questo pezzo di codice uno ad uno:

- `describe` è un blocco logico che contiene dei test intorno allo stesso topic,
- `beforeEach` è una funzione eseguita prima di ogni test (blocco `it`),
- `it` è il test vero e proprio,
- `expect` è la funzione che ci permette di effettuare __asserzioni__,
- `toBe` è uno dei tanti `matcher` offerti da jest, questo in particolare esegue un uguaglianza stretta `===`.

### vue-test-utils

`vue-test-utils` è un plugin che si occupa di aiutarci a creare ed inizializzare i nostri componenti ed a creare un ambiente dove risulti facile:

- **istanziare** un componente,
- **osservarne** l'HTML prodotto,
- **osservarne** gli eventi e le mutazioni interne,
- **manipolare** il componente come se l'utente lo stesse usando nel browser.

Purtroppo non possiamo addentrarci troppo nei dettagli di `vue-test-utils`, ma useremo le seguenti funzioni:

- `shallowMount`,
- `wrapper.exists`,
- `wrapper.props`,
- `wrapper.text`,
- `wrapper.html`,
- `wrapper.find`,
- `wrapper.findComponent`,
- `wrapper.destroy`.


## Come si testano i componenti

La parte più importante nello scrivere i test è capire cosa e come testare. Infatti, l'obeittivo è quello di raggiungere un equilibrio dove:

- le funzionalità importanti sono testate,
- i test non sono troppo prolissi e/o fragili.

Le regole d'oro da seguire sono:

1. i componenti vanno considerati `blackbox` e i loro `valori interni` non vanno mai manipolati,
    1. instanziare il componente con le necessarie prop,
    1. simulare le azioni degli utenti,
    1. verificare che l'interfaccia prodotta è corretta __oppure__,
    1. verificare che le prop corrette sono passate al/ai componente/i figlo/i,
1. non andare mai a testare il framework in sé e per se,
1. cercare di testare i componenti in isolazione, quindi utilizzeremo `shallowMount` piuttosto che `mount` il più possibile,
1. eseguire sempre i test `case` su un componente `fresco` (ricreato da zero),
1. fare particolarmente attenzione alle operazioni asincrone.


## Come strutturare un test

> Le spiegazioni a seguire sono basate sulle `best practice` utilizzate in GitLab. Se non erro GitLab ha tuttora la più grande unit test suite basata su VueJs con piu di 15000 test cases.

### Creare ed inizializzare il componente

È buona norma scrivere una funzione ad-hoc che andrà a creare il nostro componente. Tale funzione prende in input tutti i parametri (normalmente le props) necessari per l'inizializzazione del componente e assegnerà il risultato di `shallowMount` ad una variabile globale chiamata `wrapper`. Il Wrapper conterrà il nostro componente inizializzato ed una serie di funzioni che ci aiuteranno ad osservarne lo stato.

```js
describe('il mio componente', () => {
  let wrapper;

  const mountComponent = propsData => {
    wrapper = mountComponent(componentUnderTest, {propsData})
  }

  afterEach(() => {
    wrapper.destroy()
  })
})
```

Dobbiamo anche ricordarci di distruggere il componente dopo ogni test utilizzando la funzione `afterEach` messa a disposizione da `jest`

### "Trovare" i componenti e i pezzi del nostro componente
Molto spesso dovremo andare a cercare degli elementi del nostro componente (ad esempio un `button`) ed è buona norma dichiarare delle `finderFunction` all'inizio del nostro test che si occuperanno di trovare e ritornare questi elementi:


```js
describe('il mio componente', () => {
  let wrapper;

  const findMainButton = () => wrapper.find('button');

  ....

  it('has a mainButton', () => {
    mountComponent();

    expect(findMainButton().exists()).toBe(true)
  })
})
```

Una volta che la nostra `mountComponent` ha creato il nostro `wrapper` abbiamo accesso ai metodi `finder`. In questo semplice test stiamo cercando il markup `button` verificando che esista e sia visible attraverso la funzione `exists`



## Esercizio

Applicando quanto abbiamo imparato fino ad ora scriviamo una suite di test per `BaseIngredient.vue`.


## Esercizio bonus

Proviamo ora a scrivere i test per `IngredientsList.vue`. In questo caso vedremo che testare un componente __smart__ richiede più lavoro piuttosto che il test di un componente __dumb__.




