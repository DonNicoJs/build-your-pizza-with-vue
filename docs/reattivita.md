# Reattività concetti di base e dettagli importanti

La reattività (Reactivity) è uno dei concetti principali (e più confusi) quando si lavora con VueJs, vediamo ora com'è composta e come sfruttarla al meglio.

## Che cosa è?

Quando parliamo della reattività di VueJs intendiamo il sistema per cui alcune parti del componente (e dello store, ma vedremo poi) possono dipendere le une dalle altre e mantenersi aggiornate automaticamente. Se una di questa proprietà è collegata (binding) al template, anche il template (e quindi la pagina web in output) viene automaticamente aggiornato.

## Come funziona?

In VueJs 2.0 la reattività viene creata tramite una serie di `getter and setter` che vengono agganciati ad ogni proprietà reattiva (props, data, computed). Nei suoi meccanismi interni, VueJs, mantiene una lista di dipendenze e quando il `setter` di una proprietà viene invocato (ad esempio aggiornando il valore di un `input element`) tutte le proprietà che dipendon da esso vengono aggiornate.
Dalla versione 3.0 in poi vengono utilizzate tecniche differenti.

## Dettagli da tenere a mente

### La reattività ha un costo

Inserire dizionari, array troppo grandi, o strutture dati molto complesse nel sistema di reattività di VueJs ha un costo che cresce velocemente con il crescere della complessita di queste strutture. Molto spesso si tende ad utilizzare in modo eccessivo la reattività dimenticandosi che può portare a dei rallentamenti della pagina.

### Array e Object vanno manipolati differentemente

A causa di alcune limitazioni di Javascript, la reattività in VueJs (2.x) alcune operazioni su array e dizionari hanno dei limiti che vengono superati grazie ad appositi metodi.

Nello specifico, i seguenti esempi 'rompono' la reattività:

- manipolare un elemento di un array con la notazione `myArray[0] = 'something'`,
- aggiungere o modificare una chiave di un oggetto con la notazione `myObject.myNewKey = 'something'`.

I metodi `splice`, `slice`, `push`, `shift` e `unshift` negli array vengono accettati da VueJs e la reattività funziona come ci si aspetta.
Per gli oggetti possiamo:

- riassegnare con `myObject = Object.assign(myObject, {updatedValue: 'somethingNew'})`,
- riassegnare con lo spread operator: `myObject = {...myObject, updatedValue: 'somethingNew'}`,
- utilizzare Vue.set `this.$set`.

### Le computed properties hanno una cache interna

Sebbene la reattività è un'operazione costosa, le `computed` properties hanno un sistema di cache, pertanto vengono aggiornate (in memoria e nel `DOM`) solo quando necessario. Per questo dovremmo sempre preferire usare `computed` invece che `methods` ove possibile.

## Esercizio

Rivisitiamo il componente `BaseIngridient` creato nel modulo precedente e aggiungiamo le seguenti caratteristiche:

- il titolo deve essere sempre con la prima (e sola) lettera maiuscola, non importa cosa venga passato come `input`,
- se il componente riceve almeno un allergene in `input` dovremo mostrare un'icona di warning.
