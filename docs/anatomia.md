# Anatomia del componente Vue

Un componente Vue è tipicamente composto da 3 parti:

- Parte 1: `Script`,
- Parte 2: `Template`,
- Parte 3: `Style`.

Un esempio di un componente basilare potrebbe essere:

```vue
<script>
export default {
  data() {
    return {
      helloWorld: "Hello World",
    };
  },
};
</script>

<template>
  <div class="text">{{ helloWorld }}</div>
</template>

<style>
.text {
  color: lightblue;
}
</style>
```

L'ordine in cui vengono scritte le parti del componente non è vincolante ma questo è uno dei metodi più condivisi e utilizzati.

Analizziamo ora in dettaglio le tre parti.

## Parte 1: Script

Lo `script` è l'anima del componente, in esso dovremmo esportare, tramite la sintassi `export default`, un oggetto di **configurazione** che possiamo considerare
lo scheletro del nostro componente.
Vue prenderà l'oggetto generato e lo utilizzerà per instanziare il/i componenti risultanti.

L'oggetto di configurazione è composto da:

- un attributo: Props,
- una serie di opzioni:
  - Data,
  - Methods,
  - Computed,
  - Lifecycle hooks (mounted, created).

### Props

La keyword speciale `props` (abbreviazione della parola inglese "properties") è un attributo dell'oggetto di configurazione che sta ad indicare le proprietà che il componente sarà in grado di **riconoscere**. Possiamo considerare le `props` come gli **input** del nostro componente.
È importante tenere bene a mente che la comunicazione tra tutte le `props` avviene esclusivamente dall'alto verso il basso (dal componente padre al componente figlio) ma non al contrario (one-way-down binding).

Una `prop` si può definire in maniere diverse, ma la forma più completa e corretta è la seguente:

```js
export default {
  props: {
    myProperty: {
      type: [String, Number, Function, Object, Array, Boolean],
      default: null
      required: true
    }
  }
}
```

Le `prop` hanno diverse caratteristiche:

- type: definisce il tipo della propietà, Vue richiederà che la `prop` passata sia del tipo specificato. È possibile utilizzare o assegnare un solo tipo alla proprietà oppure, come nell'esempio, usare un Array (con le doppie parentesi quadre `[]`) e assegnarle più di un tipo,
- default: tramite questo attributo è possibile definire un valore di default che verrà assegnato alla `prop` qualora non le venga passato un altro valore dall'esterno,
- required: se una `prop` è marcata come `required` significa che è obbligatorio passarle un valore dall'esterno, infatti, Vue emetterà un errore nella console se non viene passato nessun valore dal componente padre.

Le **props** sono i punti di entrata basilari del nostro componente. Infatti, esse fanno parte del **API del componente** insieme al **template renderizzato** e agli **eventi emessi dal componente**.

### Data

L'opzione `data` rappresenta una funzione che conterrà tutti gli attributi, appartenenti al componente, che dovranno essere in qualche modo manipolati e resi `reattivi` da Vue.
Vedremo la reattività più nel dettaglio nelle fasi successive del corso, ma per ora basti sapere che quando una proprietà è reattiva può essere agganciata al `template` e ogni suo cambiamento è immediatamente sincronizzato nell'interfaccia.

Un esempio di funzione `data` è il seguente:

```js
export default {
  data() {
    return {
      helloWorld: "Hello Vue!",
    };
  },
};
```

La funzione `data` ha accesso a `this` e pertanto è possibile usare le `props` all'interno di essa, ad esempio

```js
export default {
  props: {
    myHello: {
      type: String,
      default: 'Vue'
      required: true
    }
  },
  data() {
    return {
      helloWorld: `Hello ${this.myHello}`
    }
  }
}
```

### Methods

Per aggiungere dei metodi all'istanza del componente viene utilizzata l'opzione `methods`. Ognuno dei metodi ha accesso a `this` e può essere invocato nel **template**, in un **altro metodo** o all'interno di una `computed` property (che vedremo più avanti).

Un esempio

```js
export default {
  methods: {
    logThis(value) {
      console.log(value, this);
    },
  },
};
```

### Computed

L'opzione `computed` contiene le proprietà del componente (definite come metodi) espresse come un'operazione o composizione di: attributi presenti in **data**, `props` oppure altre `computed`.

Il vantaggio di utilizzare una `computed` property è che, mentre un metodo definito in `methods` viene ricalcolato ogni volta che il componente viene re-renderizzato, la `computed` property utilizzerà il valore salvato nella sua cache e questo valore verrà ricalcolato solo quando ci sarà un cambiamento all'interno del componente.

In modo analogo ai metodi definiti in `methods`, anche quelli definiti all'interno di `computed` hanno accesso al `this`.

A seguire alcuni esempi:

```js
export default {
  props: {
    firstOperand: {
      type: Number,
      default: 10,
    },
  },
  data() {
    return {
      secondOperand: 2,
    };
  },
  comnputed: {
    sum() {
      return this.firstOperand + this.secondOperand;
    },
    halfSum() {
      return this.sum / 2;
    },
  },
};
```

Faremo molto utilizzo di `computed` sia a livello base che a livello avanzato.

### Lifecycle hooks

Ogni componente Vue segue un preciso ciclo di vita. Vue ci dà l'opportunità di eseguire del codice in ogni fase del ciclo di vita del componente tramite dei "ganci" chiamati `hook`.
Principalmente vengono utilizzati due `hook`:

- `mounted ` e
- `created`.

#### Mounted

Questo `hook` intercetta il momento in cui il componente e tutti i suoi figli vengono renderizzati e "agganciati" nel `DOM` eseguendo il codice scritto al suo interno.
Tipicamente questo `hook` viene utilizzato per definire manipolazioni del `DOM` o per lavorare con `canvas` (o artefatti simili).

#### Created

Questo `hook` intercetta il momento in cui il componente viene creato e prima che venga "agganciato" al `DOM` eseguendo il codice scritto al suo interno.
Normalmente è in questo `hook` che vengono eseguite operazioni di inizializzazione.

## Parte 2: Template

Se lo `script` è l'anima del componente il `template` è il suo scheletro. Il `template` contiene il markup che verrà renderizzato scritto in `HTML`, `CSS` e tramite le notazioni di binding di Vue `{{}}`.
Ogni volta che vogliamo che Vue inserisca nell'`HTML` compilato un valore dinamico possiamo **agganciare** il valore espresso in `data` o `computed` utilizzando la notazione speciale `{{}}`.

Quando viene definito un componente e il suo template bisogna tenere a mente che:

- può essere definito un solo elemento nella root (salvo eccezioni e casi d'uso avanzati),
- può essere fatto uso delle direttive basilari di Vue (spiegate nei prossimi paragrafi):
  - `v-model`,
  - `v-for`,
  - `v-if`, `v-else` e `v-else-if`,
  - `v-show`,
- è necessario utilizzare una speciale sintassi per **agganciarsi** agli eventi definita come: `@eventName`,
- è possibile utilizzare `<slot>`, ovvero un componente speciale che permette di caricare in un componente **figlio** dei contenuti del componente **padre**.

### Direttiva: v-model

`v-model` è una sintassi abbreviata per connettere il parametro (o `prop`) `value` di un elemento `DOM` (o componente) ad una proprietà del componente creando un "collegamento a due vie" (`two-way binding`). Infatti, quando la proprietà cambia anche il valore del `value` cambia e viceversa.

Ad esempio:

```vue
<script>
export default {
  data() {
    return {
      myInputValue: 'ciao';
    }
  }
}
</script>

<template>
  <input v-model="myInputValue" />
</template>
```

Appena il componente viene renderizzato, il campo `input` avrà il valore `ciao`. Se l'utente seleziona il campo `input` e ne altera il contenuto anche il contenuto di `myInputValue` verrà aggiornato in modo reattivo.

### Direttiva: v-for

Questa direttiva ci permette di generare markup attraverso un ciclo. È importante ricordare che ogni `v-for` va abbinato ad un parametro `key` che Vue usa per tenere traccia di quale parte del `DOM` viene creata da quale "pezzo" di dato.

Un esempio spiega più di mille parole:

```vue
<script>
export default {
  data() {
    return {
      littleDwarves: ['brontolo', 'pisolo', 'mammolo', 'nicolo'];
    }
  }
}
</script>

<template>
  <ul>
    <li v-for="dwarf in littleDwarves" :key="dwarf">{{ dwarf }}</li>
  </ul>
</template>
```

Questo componente costruirà il seguente markup:

```html
<ul>
  <li>brontolo</li>
  <li>pisolo</li>
  <li>mammolo</li>
  <li>nicolo</li>
</ul>
```

### Direttive v-if (v-else, v-else-if) e v-show

Queste due direttive agiscono in maniera similare, entrambe "nascondono" un pezzo del markup all'utente:

- **v-if** rimuove il markup completamente, mentre
- **v-show** appende lo stile `CSS` `style="display: none;"` all'elemento a cui è associato.

La direttiva `v-if` supporta l'utilizzo delle direttive `v-else` e `v-else-if` (non supportate da `v-show`).

Un esempio:

```vue
<script>
export default {
  data() {
    return {
      showIt: false,
    };
  },
};
</script>

<template>
  <div>
    <div v-if="showIt">NASCOSTO</div>
    <div v-if="!showIt">VISIBILE</div>
    <div v-else>NASCOSTO</div>
  </div>
</template>
```

### Event Handlers

In Vue, ogni volta che vogliamo ascoltare un evento possiamo scrivere nel `template` la sintassi `@nomeDelEvento="metodoAgganciatoAllEvento"`.
È possibile, inoltre, utilizzare dei modificatori per manipolare l'evento. Vedremo qualche caso di utilizzo di questi modificatori nel resto del corso, ma informazioni più dettagliate possono essere trovate nella documentazione ufficiale.

## Parte 3: Style

Lo `style` è il makeup del componente ed è dove viene definito il `CSS` che il componente potrà utilizzare.
È possibile utilizzare `SCSS` e `LESS` per pre-compilare il `CSS` a patto che gli appropriati loader siano installati

## Mettiamo tutti i concetti insieme

Di seguito un esempio che contiene tutti i concetti spiegati fino ad ora.

L'esempio contiene un errore, chi lo sa individuare?

```vue
<script>
export default {
  props: {
    startTimes: {
      type: Number,
      default: 0,
      required: false
    }
  },
  data: {
    helloWorld: "Hello World",
    times: this.startTimes,
  },
  computed: {
    timesInWords() {
      return `hai cliccato ${this.times} volte`;
    },
  },
  methods: {
    addToTimes() {
      this.times = this.times + 1;
    },
  },
  mounted() {
    this.times += 1;
  },
};
</script>

<template>
  <div>
    <div class="text">{{ helloWorld }}</div>
    <button @click="addToTimes">
    <span> {{timesInWords}} </span>
  </div>
</template>

<style>
.text {
  color: lightblue;
}
</style>
```

## Esercizio

Utilizzando le conoscenze imparate sin ora, creiamo un componente chiamato `BaseIngredient.vue`, tale componente deve:

- Prendere in input:
  - Nome
  - Icona
  - Lista degli allergeni
- Stampre a schermo:
  - l'icona e il nome uno di fianco all'altro
  - La Lista degli allergeni
  - Se la lista degli allergeni è vuota stampare una spunta verde
- Cliccare sul componente:
  - emette un messaggio nella console
  - emette un alert con il nome dell'ingrediente
