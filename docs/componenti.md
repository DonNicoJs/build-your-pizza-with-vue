# Comunicazione fra componenti e come "componentizzare" efficientemente

In VueJs i componenti comunicano principalmene attraverso due mezzi:

- `props` e
- `events`.

Vediamo i due casi in dettaglio

## Props

Abbiamo già visto come le `props` vengono definite all'interno dei componenti e come vengono popolate dal componente padre, ecco cosa dobbiamo tenere bene a mente:

- ogni volta che una `props` cambia all'interno del padre i componenti figli ricevono il valore aggiornato,
- le `props` sono parte del sistema di reattività e quindi hanno le stesse limitazioni descritte al [capitolo precedente](https://gitlab.com/DonNicoJs/build-your-pizza-with-vue/-/blob/main/docs/reattivita.md).

Tipicamente un componente padre mantiene dei valori all'interno di `data` e li distribuisce ai componenti figlio attraverso le `props`.

### Esercizio

Aggiungiamo una nuova `prop` al nostro componente `BaseIngredient.vue` chiamata `selected` con le seguenti caratteristiche:

- questa prop è un numero intero con **valore di default zero**,
- quando la **prop è maggiore di zero** il componente mostra il valore di `selected` accanto al nome.

## Eventi

Gli eventi sono la forma di comunicazione "verso l'alto" di VueJS. Quando un componente emette degli eventi, il padre può agganciarsi ad essi ed eseguire uno o piu metodi in risposta.

> Chi ricorda quale è la sintassi per emettere un evento in VueJs?

### Esercizio

Apriamo il componente `BaseIngredient.vue` che abbiamo creato e modifichiamo il `click handler` per emettere un evento `click` verso l'alto.

## Refs

VueJs supporta la possibilità di referenziare un altro componento e/o un elemento del `DOM` tramite le `refs`. Effettuato questo collegamento è possibile richiamare un metodo dell'elemento referenziato.

### Esempio

Consideriamo il seguente caso (molto comune)

```vue
<script>
export default {
  mounted() {
    // lifecycle hook eseguito quando il componente è stampato nel markup
    this.$refs.inputRef.focus();
  },
};
</script>

<template>
  <input ref="inputRef" />
</template>
```

Quando questo componente viene renderizzato ed è pronto, la funzione `focus` dell'input viene chiamata e l'elemento va in focus.

> Le `ref` vengono utilizzate ogni volta che vogliamo chiamare una funzione su un elemento figlio in maniera programmatica. Generalmente non vanno usate per passare informazioni. Degli usi tipici sono: focus, mostrare/nascondere modali.

## Mini Schema

```mermaid
  graph TD
    A(Padre) -->|Passa Props| B(Figlio)
    B --> |Emette Eventi| A
    A --> |Richiama metodi con refs| B
```

## Errori tipici nella comunicazione fra componenti

Degli errori tipici che vengono fatti nella comunicazione fra componenti sono:

- un componente figlio non deve mai aggiornare una prop direttamente,
- un componente padre non deve mai andare a manipoalare il markup/dati di un componente figlio,
- oggetti e Array non devono essere mai aggiornati dal componente figlio.

## Classificare i componenti

Lavorando con i componenti, in particolare in VueJS, è importante approcciare il lavoro con un concetto ben chiaro: non tutti i componenti hanno lo stesso livello di complessità, ne devono averlo.
Una classificazione molto semplice ma importante è quella di componenti **smart** e componenti **dumb** vediamone le caratteristiche.

### Dumb components

I `Dumb components` sono componenti che:

- non hanno molta logica,
- hanno molto markup e stile,
- di norma **visualizzano un concetto o funzionalità** ,
- hanno degli unit test semplici che si concentrano sul markup generato.

#### Esempio

Il componente `BaseIngredient.vue` che abbiamo creato è un classico componente _dumb_. La sua logica è prettamente presentazionale: prende le prop e stampa a schermo i loro input in un certo layout predeterminato.

#### Esercizio

Creiamo un nuovo componente, tenendo a mente che deve essere un componente _dumb_ lo chiameremo `BaseAllergens.vue`.

Tale componente:

- prende in input un allergene (il nome),
- prende in input un'icona (il nome),
- stampa a schermo il nome dell'allergene e l'icona uno di fianco all'altra.

### Smart components

Gli `smart components`:

- contengono la logica di busines,
- si occupano di comunicare con la API,
- orchestrano molteplici componenti **dumb**,
- non implementano l'interfaccia, delegandola ai componenti **dumb**,
- normalmente hanno degli unit test complicati e fatti da numerosi step.

#### Esempio

Il tipico componente _smart_ da manuale è un componente che si occupa ad esempio di mostrare una lista, contiene svariati componenti _dumb_ e si connette alle API (o quantomeno mantiene in memoria uno stato o snapshot).

#### Esercizio

Creiamo un nuovo componente, tenendo a mente che deve essere un componente _smart_, chiamato `IngredientList.vue`.

Tale componente:

- crea una lista di `BaseIngredient` : `['Farina', 'Acqua', 'Pomodoro', 'Mozzarella', 'Salame', 'Zucchine', 'Lievito']`,
- quando `BaseIngredient` emette l'evento `click` memorizza l'ingrediente come `selezionato`,
- quando un ingrediente viene selezionato, la prop `selected` di `BaseIngredient` va valorizzata correttamente.

> Bonus: possiamo riscrivere questo comportamento (`click event` e `selected prop`) tramite l'uso di `v-model`; vedremo come durante l'esercizio.

## Il principio di Single Responsibility

Questo è probabilmente il concetto più importante da tenere in mente quando lavoriamo con dei componenti. Dobbiamo sforzarci il più possibile nel creare un solo componente che si occupi di un'attività ben definita essendone il responsabile.
Questo non significa creare dei componenti mostruosi con duecento props, trecento eventi, e tremila linee di codice, questo significa individuare un concetto e creare un componente intorno ad esso, utilizzando le tecnice messe a disposizione da VueJs
per combinare i componenti creati.

In GitLab utiliziamo un approccio iterativo che io ritengo molto valido in generale nel lavorare con il frontend e in particolare con i componenti. Ecco un esempio di come si svolge il processo decisionale che utilizzo quotidianamente:

```mermaid
  graph TD

  A(Creo il componente Titolo) --> | Richiesta di aggiungere un avatar al componente titolo | B(Componente titolo con Avatar)
  B -->  C(Creo il componente ElementoLista)
  C --> |Richiesta di aggiungere un avatar al componente Elemento Lista| D(Creo il componente Avatar)
  D --> E(Utilizzo il componente Avatar in Elemento Lista)
  D --> F(Utilizzo il componente Avatar nel componente Titolo)
```

Il `Single Responsibility Principle` ci aiuta a creare dei componeti con delle API precise, decidere quali funzionalità un componente debba avere e sopratutto a scrivere gli opportuni unit test.

> La combinazione di questo concetto ci aiuta ad avere la confidenza nel riusare il codice già scritto (da noi o altri) senza preoccuparsi di indagare se, come e quando funzioni.

### Esercizio

Dobbiamo ora aggiungere un'icona al componente `BaseAllergens`. In aggiunta, il nostro UX department ha deciso che ogni icona deve sempre avere `4px` di margine su tutti i lati, non importa dove questa icona sia. Di conseguenza:

- creiamo un componente `BaseIcon.vue` che mostra una immagine di icona con il padding richiesto,
- utiliziamo gli `slot` di vue per inserire `BaseIcon.vue` in `BaseAllergens` e `BaseIngredient`.
