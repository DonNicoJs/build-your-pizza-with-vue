# Pattern base di architettura: da semplici app a complesse SPA
 
Uno dei motivi per cui VueJs è molto apprezzato è la sua versatilità e al fatto che si presti sia a semplici widgets che a complesse app.
 
## Veloce e facile
 
Per iniziare ad usare VueJs non è necessario usare webpack o gli `SFC` ci basta aggiungere uno script alla pagina:  
 
`<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>`
 
E possiamo immediatamente iniziare a scrivere componenti ed utilizzarli, vediamo un esempio preso direttamente dalla guida ufficiale: 
 
```html
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
 
<div id="app">
  {{ message }}
</div>
 
<script>
  const app = new Vue({
    el: '#app',
    data: {
      message: 'Ciao Vue!'
    }
  })
</script>
```
 
In ordine:
 
- carichiamo la versione `dev` di VueJs nella pagina,
- aggiungiamo un `div` con un `id` a cui collegheremo la app vue,
- aggiungiamo uno script che richiama il costruttore di `Vue` passandogli l'`id` dell'elemento dove deve connettersi.
 
## Architettura tipica
 
Normalmente, aggiungere uno script alla pagina non è abbastanza per sviluppare funzionalità più complesse, è per questo che la maggior parte dei progetti fatti con Vue utilizzano la `vue cli`, utilizzata per creare un progetto basato su webpack.
In questi progetti avremo a disposizione i Vue `Single File Component` (`SFC` in breve), sono gli stessi file che abbiamo utilizzato fino ad ora nei nostri esempi e contegono `template`, `script` e `style` del componente.
Quando il codice viene compilato, VueJs, attraverso webpack e gli appositi loader, converte gli `SFC` in file `js` e `css`. Questo diviene ancora più importante quando vogliamo iniziare ad usare uno state manager e/o un router (nel caso di VueJs vengono chiamati rispettivamente `Vuex` e `vue-router`)
 
Ritorniamo alla nostra app per creare pizze personalizzate, il nostro cliente vuole espandere il suo business e ora offre anche pasta personalizzata. Vuole aggiungere questa nuova funzionalità alla sua app per customizzare la pizza, vediamo come procedere per ideare l'architettura.
 
I componenti che abbiamo costruito fino ad ora e che possono essere riutilizzati sono:
 
- il componente per gli ingredienti,
- il componente per gli allergeni,
- il componente per le icone,
- il componente per creare liste (dobbiamo rimuovere gli ingredienti 'hard coded' ma per il resto possiamo utilizzarlo).
 
Per soddisfare i nuovi requisiti dobbiamo solo introdurre il concetto di `pagina` nella nostra applicazione, una pagina per `pizza` e una per `pasta` che passeranno le necessarie configurazioni ai componenti sottostanti.
Per fare ciò possiamo, data la semplicità dell'applicazione, semplicemente utilizzare `v-if` per mostrare una pagina o l'altra.
 
### Esercizio
 
Per aggiungere le nuove funzionalità all'app del nostro cliente sarà sufficiente svolgere i seguenti passi:
 
- creiamo un nuovo componente `PizzaView.vue` che carica i componenti che abbiamo già creato aggiungendo il titolo pizza,
- creiamo un nuovo componente `PastaView.vue` che carica i componenti che abbiamo già creato aggiungendo il titolo pasta,
- modifichiamo il file `Home.vue` contenuto nella cartella `views` nella seguente maniera:
  - aggiungiamo due bottoni, uno per pizza e uno per pasta,
  - quando `pizza` è selezionato tramite `v-if` mostriamo il componente `PizzaView`,
  - quando `pasta` è selezionato tramite `v-else-if` mostriamo il componente `PastaView`,
  - quando né pasta né pizza è selezionato mostriamo una scritta `Prego selezionare pasta o pizza`.
 
 
## Construire interfacce: assemblare mattoncini
 
È già visibile un pattern ben preciso sul come construire interfacce efficientemente con VueJs: combinare in maniera efficente __smart__  e __dumb__ components, utilizzando una manciata di __smart__ components per coordinare e orchestrare molti componenti __dumb__ 
 
```mermaid
  flowchart TD
  A(Componente Smart A) --> B(Componente Dumb B)
  A --> C(Componente Dumb C)
  A --> D(Componente Dumb D)
  F(Componente Smart F) --> D(Componente Dumb D)
  F --> G(Componente Dumb G)
  F --> H(Componente Dumb H)
```
 
Spesso sussiste la necessità di avere due componenti `smart` in comunicazione (un esempio sono i componenti A ed F nel nostro grafico) in modo che possano scambiare dati gli uni con gli altri. Esistono diverse possibilità per creare un'architettura di questo tipo.
 
### Spostare la comunicazione un livello in alto
 
Invece che mettere A ed F in comunicazione possiamo spostare la responsabilità della comunicazione ad un altro componente `smart` che li contiene entrambi:
 
 
```mermaid
  flowchart TD
  I(Componente Smart I) -- Passa Props --> A
  I  -- Passa Props --> F
  A  -- Emette eventi --> I
  F  -- Emette eventi --> I
  A(Componente Smart A) --> B(Componente Dumb B)
  A --> C(Componente Dumb C)
  A --> D(Componente Dumb D)
  F(Componente Smart F) --> D(Componente Dumb D)
  F --> G(Componente Dumb G)
  F --> H(Componente Dumb H)
 
```
  
Così facendo diamo la responsabilità al componente I di gestire il dialogo fra A ed F. Questo non sempre è possibile e qualche volta risulta in lunghe catene di `props` ripetute e "rimbalzate" di componente in componente.
 
#### Esercizio
Ora vogliamo che i nostri utenti possano utilizzare un ingrediente o per la pasta o per la pizza, ossia, se l'utente seleziona `pomodoro` per la pizza non può utilizzare l'ingrediente `pomodoro` per la sua pasta (poverino!). Per ottenere questo questo risultato:
 
- aggiungiamo un valore `selectedForPizza` e `selectedForPasta` nel nostro `Home.vue`,
- quando `PastaView` o `PizzaView` seleziona un ingredienti aggiorniamo la collezione appropriata,
- filtriamo gli ingredienti per Pasta e Pizza tramite computed properties,
- aggiungiamo a `Pasta e Pizza View` una `allowed ingredient` prop che controlla quale ingredienti gli utenti possono usare,
- utilizziamo la nuova prop nelle nostre liste.
 
### Utilizzare uno state manager
 
Ogni applicazione di una certa dimensione ha un set di dati (che chiameremo `stato`) che si ripete in molte aree. 
 
Ad esempio lo `stato` rappresenta:
 
- il 'valore' della variabile utente,
- configurazioni generali della app,
- dati che vengono estratti dalle API di un server.
 
Invece che mantenere questi dati in un componente `root` e farli "scivolare" su tutti i componenti attraverso le `props` possiamo usare uno state manager (`Vuex` è lo state manager sviluppato per VueJs ma ce ne sono altri come ad esempio `vue-apollo`).
 
Vedremo `Vuex` in dettaglio nel prossimo capitolo, ma introducendo uno state manager la comunicazione unidirezionale fra i componenti ora include anche una comunicazione bidirezionale verso lo `store` o `state manager`.
 
Sebbene sia possibile far comunicare qualsiasi componente con lo store il mio consiglio è sempre di delegare questa comunicazione ai componenti __smart__ e promuovere qualsiasi componente __dumb__ a __smart__ qualora sia necessario collegarlo allo store.
 
> Nota: nulla vieta che un componente __smart__ contenga uno o più componenti __smart__.
 
Vediamo uno schema di come funziona il flusso dei dati utilizzando uno `store` o `state manager`:
 
```mermaid
  flowchart TD
  I(Store) <-- Scambia Dati --> A
  I <-- Scambia Dati -->  F
  A(Componente Smart A) --> B(Componente Dumb B)
  A --> C(Componente Dumb C)
  A --> D(Componente Dumb D)
  F(Componente Smart F) --> D(Componente Dumb D)
  F --> G(Componente Dumb G)
  F --> H(Componente Smart H)
  H <-- Scambia Dati ---> I
 
```
 
Lo scambio di dati avviene tramite un sistema di `binding` dove nel componente andiamo a collegare delle porzioni dello `stato` a delle `computed properties`. Quando vogliamo modificare lo `stato` richiamiamo dei metodi speciali che aggiornano il valore nello `state manager`.