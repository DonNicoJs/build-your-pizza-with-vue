# Vue router e Vuex: introduzione

## Vue Router

Nel capitolo precedente abbiamo creato due `pagine` con l'utilizzo di `v-if` e un bottone. Così facendo abbiamo creato un rudimentale set di pagine la cui funzionalità è però molto basilare, ossia non è connessa alla URL e non supporta delle sotto pagine.
Per costruire una app che supporti la navigazione tra pagine possiamo però utilizzare `vue-router`, il router ufficiale di VueJs. 

Per far ciò dovremo:

1. **installare** `vue-router`, 
1. **creare** il file di configurazione del `router`,
1. **creare** una o più `pagine/views`,
1. **importare** il `router` nella nostra applicazione.

Questi step vengono eseguiti automaticamente utilizzando il comando `vue add router` all'interno di `vue-cli`. 

### Configurare il router

`Vue-router` necessita principlamente di:

- un file di configurazione che specifichi le pagine che vogliamo utilizzare,
- le pagine, sotto forma di `SFC`,
- un componente speciale, chiamato `router-view` dove le pagine, o le sottopagine, verranno caricate.

### Il file di configurazione

Il file di configurazione è composto da due parti principali:
- la definizione delle proprietà generali e
- la definizione delle delle routes. 

Vediamolo passo dopo passo:

```javascript
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [],
})
```

In questa porzione di codice: 

1. invochiamo il costruttore del `router`,
1. passiamo un `oggetto di configurazione` dove specifichiamo le 3 proprietà fondamentali:
   1. la modalità del `router` (se utilizza la history API o le query string per la navigazione),
   1. la `url di base`, ovvero la parte `sinistra` e immutabile dell'url,
   1. le `routes` che vogliamo configurare.

Vediamo ora come configurare una route:


```javascript
const routes = [
  {
    path: "/ingredients",
    name: "Ingredients",
    component: () =>
      import(/* webpackChunkName: "Ingredients" */ "../views/Ingredients.vue"),
  },
];
```

`routes` è sempre un array di oggetti ed ogni oggetto deve avere le seguenti proprietà:

1. `path`, ovvero che cosa verrà scritto e riconosciuto nalla URL,
1. `name`, ovvero il nome della route, che sarà utilizzato per _riconoscere_ la route quando navigheremo programmaticamente,
1. `component`, ovvero il componente che verrà caricato quando l'URL sarà visitata.

### Approfondimenti

`Vue-router` implementa altri tre importanti funzionalità:

- le sotto pagine,
- le route dinamiche,
- le `guardie` pre e post navigazione.

Purtroppo ognuno di questi argomenti richiede molto tempo per essere approfondito, ma la documentazione ufficiale è molto completa e di facile consultazione.

### Esercizio

- Transformiamo `PastaView` e `PizzaView` in routes di vue-router,
- usiamo `<router-link>` per navigare da una pagina all'altra.

## Vuex 

Nel capitolo precedente abbiamo introdotto il concetto di `state manager/store`. Lo state manager di vue si chiama `Vuex` e per utilizzarlo dobbiamo: 

1. **installare** `vuex`,
1. **creare** un `file di configurazione` per lo store,
1. **creare** uno `stato`, delle `azioni` e delle `mutazioni`,
1. **importare** ed utilizzare il `router` nella nostra applicazione,

Questi step vengono eseguiti automaticamente tramite il comando `vue add vuex` su `vue-cli`.

> Lo store è un `singleton` e viene istanziato una sola volta alla creazione della app.

### L'anatomia dello store

Uno store è composto da due parti principali:

- `state`: i dati contenuti all'interno dello `store`,
- `mutation`: funzioni pure e sincrone che aggiornano lo `state`.

E da due parti complementari:

- `actions`: funzioni che richiamano (`commit`) le mutazioni o altre actions (`dispatch`),
- `getters`: funzioni che aggregano uno o più porzioni dei dati presenti nello `stato`.

### Configurazione di Vuex

```js
export default new Vuex.Store({
  state: {
    ingredients: ['ciabatte', 'pneumatici']
  },
  mutations: {
    SET_INGREDIENTS: (state, ingredients) => {
      state.ingredients = [...ingredients]
    }
  },
  actions: {
    updateIngredients: ({commit}, updatedIngredients) => {
      doSomeAsyncTask();
      commit('SET_INGREDIENTS', updatedIngredients)
      dispatch('anotherActionName')
    }
  },
  getters: {
    firstIngredient: (state) => {
      return state.ingredients[0] || {}
    }
  },
});
```

Comunemente `actions`, `mutations` e `getters` vengono scritte su file separati e poi importati nel file di configurazione principale.

#### Esercizio

Utilizzando la base dello store già presente nel repository creiamo:

- all'interno dello `state`: 
  - la lista degli ingredienti,
  - la lista degli ingredienti selezionati per la pasta,
  - la lista degli ingredienti selezionati per la pizza,
- una `mutazione` per aggiornare la lista degli ingredienti selezionati,
- una `action` per richiamare la mutazione che aggiorna gli ingredienti,
- un `getter` per gli ingredienti disponibili per la pizza,
- un `getter` per gli ingredienti disponibile per la pasta.


### Utilizzare vuex nei componenti

Una volta configurato e inizializzato lo `store` è possibile agganciarlo ai componenti in diverse maniere. Il metodo più comune e che fa parte delle `best practices` è quello di utilizzare le funzioni di `mapping`: `mapGetters`, `mapState` e `mapActions`. 

Vediamole in dettaglio.

#### mapGetters

`mapGetters` viene utilizzata nelle `computed properties` del componente e serve a connettere un `getter` ad una proprietà del componente:

```js
import { mapGetters } from 'vuex';

export default {
  computed: {
    ...mapGetters(['firstIngredient'])
  }  
}
```

Cosi facendo il nostro componente avrà una `computed` property aggiuntiva connessa al valore conservato nello `store`. Ogni volta che lo `stato` cambia la proprietà verrà aggiornata di conseguenza.

> Le proprietà all'interno di `vuex` sono **reattive**, come le proprietà all'interno dei componenti, e pertanto abbiamo le stesse limitazioni descritte nel capitolo [Reattività](https://gitlab.com/DonNicoJs/build-your-pizza-with-vue/-/blob/main/docs/reattivita.md).

#### mapState

`mapState` viene utilizzata nelle `computed properties` del componente e consente di connettere direttamente allo `stato` conservato nello `store`:

```js
import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState(['ingredients'])
  }  
}
```

Cosi facendo il nostro componente avrà una `computed` property aggiuntiva connessa al valore conservato nello `store` e ogni qual volta lo `stato` cambia la proprietà verrà aggiornata di conseguenza.

#### mapActions

`mapActions` viene utilizzato nei `methods` del componente e aggancia le funzioni descritte in `actions` nello `store` al componente:


```vue
<script>
  import { mapActions } from 'vuex';

  export default {
    methods: {
      ...mapActions(['updateIngredients'])
    }  
  }
</script>

<template>
  <button @click="updateIngredients(['Nutella', 'Fragole', 'Salame Piccante'])">Make Nico's favourite pizza </button>
</template>
```

#### Esercizio

Connettiamo ora i nostri componenti allo `store` utilizzando le funzioni sopra descritte.










